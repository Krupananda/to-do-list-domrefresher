let data = [];

function domRefresh() {
    document.getElementsByTagName('ul')[0].innerHTML = "";
    for (var i = 0; i <= data.length - 1; i++) {
        let list = document.createElement("li");
        list.setAttribute("track", i);
        let box = document.createElement("input");
        box.type = 'checkbox';
        box.checked = data[i].status;
        box.onclick = toggleTodoStatus;
        let label = document.createElement("label");
        let bttn = document.createElement("button");
        let message = "";
        if (data[i].status) {
            message = "<strike>" + data[i].text + "</strike>";

        } else {
            message = data[i].text;
        }
        label.innerHTML = message;
        bttn.textContent = "X";
        bttn.onclick =  deleteTodoItem;

        list.append(box);
        list.append(label);
        list.append(bttn);
        document.getElementsByTagName("ul")[0].appendChild(list);
    }
}

document.getElementsByTagName("input")[0].addEventListener("keypress", function(e) {
    addTodoItem(e);
});

function addTodoItem(e) {

    if (e.keyCode == 13 && e.target.value != "") {
        console.log(e.target.value);
        data.push({
            status: false,
            text: e.target.value

        });
        e.target.value = "";
        domRefresh();

    }
}

function toggleTodoStatus() {
    let box = event.target.parentElement.getAttribute('track');
    if (data[box].status) {
        data[box].status = false;
    } else {
        data[box].status = true;
    }
    domRefresh();

}

function deleteTodoItem(event) {
    event.preventDefault();
    event.stopPropagation();
    let remove = event.target.parentElement.getAttribute('track');
    data.splice(remove, 1);
    domRefresh();
}